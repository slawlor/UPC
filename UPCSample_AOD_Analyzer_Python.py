#!/usr/bin/env python

# Set up ROOT
import ROOT

from ROOT import TCanvas, TColor, TGaxis, TH1F, TPad
from ROOT import kBlack, kBlue, kRed, kFullCircle

#Import standard
import math, numpy, array, sys, os
import pickle
from itertools import product

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

import mplhep as hep
hep.set_style(hep.style.ROOT)

from matplotlib import gridspec
import matplotlib.lines as mlines


###################################
######## dummy function ###########
######## to convert TH1 ###########
### to list  and ratio functions ##
##################################

def ConvertTH1_to_List(xmin, xmax, nBins, hist):
  hTemplate = copy.deepcopy(hist)
  #Define massList
  List = []
  histrange = np.linspace(xmin, xmax, nBins)
  ### if you start from 0, the x is from 150
  for i in range(0,len(histrange)):
    #### y taken from histogram
    y = hTemplate.GetBinContent(i)
    # print("x: ", x, " y: ", y)
    List.append(y)

  return List 


##################################
##################################

Directory = "/home/lawlor/UPC_local/"

import argparse
parser = argparse.ArgumentParser(description='Batch Selections')
parser.add_argument('-datatype', metavar='', help='run background or signal')
parser.add_argument('-numberevents', metavar='', help='run all events or first N for debugging [all,N]')
parser.add_argument('-printout', metavar='', help='plot in ROOT or python [yes,no]')
parser.add_argument('-pythonplot', metavar='', help='make python list first, then you can quickly plot with python [makeinputs,plot]')
argus = parser.parse_args()




if(argus.pythonplot=="makeinputs"):

    # Initialize the xAOD infrastructure 
    if(not ROOT.xAOD.Init().isSuccess()): print("Failed xAOD.Init()")

    # Set up the input files
    #Background
    if(argus.datatype=="bkg"):
        fileName = Directory+"Samples/MC/mc15_5TeV.860156.StarlightPy8_gammagamma2ee_1p8M.recon.AOD.e8467_s3894_r14340/AOD.32638317._Merged.pool.root.1"
    #Signal
    if(argus.datatype=="sig"):
        fileName = Directory+"Samples/MC/mc21_5p36TeV.860168.SuperChic_4p15_gammagamma2gammagamma_2M500.recon.AOD.e8515_e8455_s3894_r14340/AOD.33193275._Merged.pool.root.1"

    treeName = 'CollectionTree'
    f = ROOT.TFile.Open(fileName)

    # Make the "transient tree" and load any iterators we may need
    import xAODRootAccess.GenerateDVIterators
    t = ROOT.xAOD.MakeTransientTree(f, treeName)


    # Setup the output histogram file
    if(argus.datatype=="bkg"):
        f_out = ROOT.TFile('UPC_xAODAnalysis_Output_StarlightPy8_gammagamma2ee_1p8M.recon.AOD.e8467_s3894_r14340.root', 'recreate')
    if(argus.datatype=="sig"):
        f_out = ROOT.TFile('UPC_xAODAnalysis_Output_SuperChic_4p15_gammagamma2gammagamma_2M500.root', 'recreate')

    h_el_pt = ROOT.TH1F('el_pt', 'el_pt', 20, 0, 20)
    h_el_eta = ROOT.TH1F('el_eta', 'el_eta', 100, -3, 3)
    h_el_phi = ROOT.TH1F('el_phi', 'el_phi',  100, -2, 2)

    h_ph_pt = ROOT.TH1F('ph_pt', 'ph_pt', 20, 0, 20)
    h_ph_eta = ROOT.TH1F('ph_eta', 'ph_eta', 100, -3, 3)
    h_ph_phi = ROOT.TH1F('ph_phi', 'ph_phi',  100, -2, 2)

    truth_h_el_pt = ROOT.TH1F('truth_el_pt', 'truth_el_pt', 20, 0, 20)
    truth_h_el_eta = ROOT.TH1F('truth_el_eta', 'truth_el_eta', 100, -3, 3)
    truth_h_el_phi = ROOT.TH1F('truth_el_phi', 'truth_el_phi',  100, -2, 2)

    truth_h_ph_pt = ROOT.TH1F('truth_ph_pt', 'truth_ph_pt', 20, 0, 20)
    truth_h_ph_eta = ROOT.TH1F('truth_ph_eta', 'truth_ph_eta', 100, -3, 3)
    truth_h_ph_phi = ROOT.TH1F('truth_ph_phi', 'truth_ph_phi',  100, -2, 2)

    #Make some histograms for efficiencies

    h_el_pt_efficiency = ROOT.TH1F('el_pt_efficiency', 'el_pt_efficiency', 20, 0, 20)
    h_el_eta_efficiency = ROOT.TH1F('el_eta_efficiency', 'el_eta_efficiency', 100, -3, 3)
    h_el_phi_efficiency = ROOT.TH1F('el_phi_efficiency', 'el_phi_efficiency',  100, -2, 2)

    h_ph_pt_efficiency = ROOT.TH1F('ph_pt_efficiency', 'ph_pt_efficiency', 20, 0, 20)
    h_ph_eta_efficiency = ROOT.TH1F('ph_eta_efficiency', 'ph_eta_efficiency', 100, -3, 3)
    h_ph_phi_efficiency = ROOT.TH1F('ph_phi_efficiency', 'ph_phi_efficiency',  100, -2, 2)


    GeV = 1000

    #Choose a smaller number for debug runs - used when numberevents = "debug", use "all" to run over all
    debug_numberevents = 500
    numberevents = str(argus.numberevents)

    #Count amount of reco and truth particles that pass our selections
    total_number_el = []
    total_number_ph = []
    if(argus.datatype=="bkg"):
        el_pt_values = []
        truth_el_pt_values = []
    if(argus.datatype=="sig"):
        ph_pt_values = []
        truth_ph_pt_values = []
    total_truth_number_el =  []
    total_truth_number_ph =  []

    # Print some information
    if(argus.numberevents=="debug"):
        print("Not using total amount of events - DEBUG MODE - number of input events being used ",debug_numberevents)
    else:
        print("Using all input events: ",t.GetEntries())



    #Begin eventloop
    for entry in range(t.GetEntries()):

        if(argus.numberevents=="debug"):
            if(entry>debug_numberevents): break

        t.GetEntry(entry)


        if(argus.numberevents=="debug"):
            s = 'Processing run #%i, event #%i' % (t.EventInfo.runNumber(), t.EventInfo.eventNumber())
            # print(s)

        if(argus.datatype=="bkg"):
            #Background Sample yy->ee: need different treatment than yy final state - look for ee efficiency - find reco ee pair and truth ee through elimnation - to do: isPrompt
            truth_particle_list = []
            truth_getBkgElectronLineage_list = []
            # Loop over electrons
            for el in t.Electrons:
                #which truth particle matches the reco particle
                el_truthmatched = ROOT.xAOD.TruthHelpers.getTruthParticle(el)
                truth_getBkgElectronLineage_list.append(ROOT.xAOD.EgammaHelpers.getBkgElectronLineage(el))
                lastElTruth = ROOT.xAOD.EgammaHelpers.getBkgElectronMother(el)
                if not el_truthmatched==None and not lastElTruth==None:
                    if abs(lastElTruth.pdgId())==22 or abs(lastElTruth.pdgId())==11:
                        # print("el_truthmatched particle", el_truthmatched.pdgId())
                        # print("el_truthmatched", el_truthmatched)
                        total_number_el.append(el)
                        #Obtain information about each electron
                        el_pt = el.pt()/GeV
                        el_eta = el.eta()
                        el_phi = el.phi()
                        # print("el_pt",el_pt) 
                        # print("el_eta",el_eta) 
                        # print("el_phi",el_phi)    
                        el_m = el.m()/GeV
                        #Fill electron histograms
                        h_el_pt.Fill(el_pt)
                        h_el_eta.Fill(el_eta)
                        h_el_phi.Fill(el_phi)
                        el_pt_values.append(el_pt)
                    

            #Truth el
            #Once we have seen all possible vertices per event and the signal vertex we can decide if the event is one we want to store for the eff calc
            for n in range(t.TruthEvents.size()):
                vertices_ofinterest = []
                event = t.TruthEvents[n]
                trackParticle = event.truthParticleLinks()
                # print("####################")
                # print("Will use signalProcessVertex to look most likely signal vertex....")
                TruthEvent_signalProcessVertex = event.signalProcessVertex()
                truth_el_vtx_In_signalProcessVertex = TruthEvent_signalProcessVertex.nIncomingParticles()
                truth_el_vtx_Out_signalProcessVertex = TruthEvent_signalProcessVertex.nOutgoingParticles()
                # print("truth_el_vtx_In_signalProcessVertex - nIncomingParticles for signalProcessVertex from "+str(event)+": ",truth_el_vtx_In_signalProcessVertex)
                # print("truth_el_vtx_Out_signalProcessVertex - nOutgoingParticles for signalProcessVertex from "+str(event)+": ",truth_el_vtx_Out_signalProcessVertex)
                # print("Loop over signalProcessVertex incomingParticle....")
                for i in range(truth_el_vtx_In_signalProcessVertex):
                    truth_el_vtx_incomingParticle = TruthEvent_signalProcessVertex.incomingParticle(i)
                #     print("Particle "+str(i+1)+" - signalProcessVertex_incomingParticle type: ",truth_el_vtx_incomingParticle.pdgId())
                #     print("Particle "+str(i+1)+" - signalProcessVertex_incomingParticle_Barcode: ",truth_el_vtx_incomingParticle.barcode())
                #     print("Particle "+str(i+1)+" - signalProcessVertex_incomingParticle_Index: ",truth_el_vtx_incomingParticle.index())
                #     print("Particle "+str(i+1)+" - signalProcessVertex_incomingParticle_Status",truth_el_vtx_incomingParticle.status())
                # print("Loop over signalProcessVertex outgoingParticle")
                for i in range(truth_el_vtx_Out_signalProcessVertex):
                    truth_el_vtx_outgoingParticle = TruthEvent_signalProcessVertex.outgoingParticle(i)
                    # try:
                    #     print("Particle "+str(i+1)+" - signalProcessVertex_outgoingParticle:",truth_el_vtx_outgoingParticle.pdgId())
                    #     print("Particle "+str(i+1)+" - signalProcessVertex_outingParticle_Barcode:",truth_el_vtx_outgoingParticle.barcode())
                    #     print("Particle "+str(i+1)+" - signalProcessVertex_outingParticle_Index:",truth_el_vtx_outgoingParticle.index())
                    #     print("Particle "+str(i+1)+" - signalProcessVertex_outingParticle_Status:",truth_el_vtx_outgoingParticle.status())
                    # except:
                    #     print("Particle "+str(i+1)+" - attempted to access a null-particle")
                #Grab TruthVertices objects that are useful    
                for v in range(t.TruthVertices.size()):
                    #Want single vertex
                    # print("####################")
                    # print("####################")
                    # print("####################")
                    # print("vertex_number: ",v+1)
                    truth_el_vtx = t.TruthVertices[v]
                    truth_el_vtx_In = truth_el_vtx.nIncomingParticles()
                    truth_el_vtx_Out = truth_el_vtx.nOutgoingParticles()
                    vertices =(truth_el_vtx_In,truth_el_vtx_Out)
                    vertices_ofinterest.append(vertices)
                    # print("truth_el_vtx_In - nIncomingParticles for vertex "+str(v+1)+": ",truth_el_vtx_In)
                    # print("truth_el_vtx_Out - nOutgoingParticles for vertex "+str(v+1)+": ",truth_el_vtx_Out)
                    # print("Loop over incomingParticle....")
                    for i in range(truth_el_vtx_In):
                        truth_el_vtx_incomingParticle = truth_el_vtx.incomingParticle(i)
                        # print("Particle "+str(i+1)+" - truth_el_vtx_incomingParticle type: ",truth_el_vtx_incomingParticle.pdgId())
                        # print("Particle "+str(i+1)+" - truth_el_vtx_incomingParticle_Barcode: ",truth_el_vtx_incomingParticle.barcode())
                        # print("Particle "+str(i+1)+" - truth_el_vtx_incomingParticle_Index: ",truth_el_vtx_incomingParticle.index())
                        # print("Particle "+str(i+1)+" - truth_el_vtx_incomingParticle_status",truth_el_vtx_incomingParticle.status())

                        truth_el_vtx_outgoingParticle = truth_el_vtx.outgoingParticle(i)
                    # print("Loop over outgoingParticle")
                    for i in range(truth_el_vtx_Out):
                        truth_el_vtx_outgoingParticle = truth_el_vtx.outgoingParticle(i)
                        # try:
                        #     print("Particle "+str(i+1)+" - truth_el_vtx_outgoingParticle",truth_el_vtx_outgoingParticle.pdgId())
                        #     print("Particle "+str(i+1)+" - truth_el_vtx_outingParticle_Barcode",truth_el_vtx_outgoingParticle.barcode())
                        #     print("Particle "+str(i+1)+" - truth_el_vtx_outingParticle_Index",truth_el_vtx_outgoingParticle.index())
                        #     print("Particle "+str(i+1)+" - truth_el_vtx_outingParticle_status",truth_el_vtx_outgoingParticle.status())

                        # except:
                        #     print("Particle "+str(i+1)+" - attempted to access a null-particle")
                    # print("####################")
                    # print("####################")

                #Check for 2 particles coming in/out
                # print(vertices_ofinterest)
                # while (2,2) in vertices_ofinterest:
       
                truth_particle_number = []

                for truth_getBkgElectronLineage in truth_getBkgElectronLineage_list:
                    for truth_particle in truth_getBkgElectronLineage:
                        #Grab TruthParticles objects that are useful
                        particleId = truth_particle.pdgId()
                        particle_status = truth_particle.status()
                        hasDecayVtx = truth_particle.hasDecayVtx()
                        hasProdVtx = truth_particle.hasProdVtx()
                        truth_el_barcode = truth_particle.barcode()
                        truth_child = truth_particle.child()
                        truth_parent = truth_particle.parent()
                        truth_numberchild = truth_particle.nChildren()
                        truth_numberparent = truth_particle.nParents()
                        #Dont exist?
                        # truth_isprompt = truth_particle.isPrompt()
                        # truth_fromdecay = truth_particle.isFromDecay()
                        #Create list of truth particles
                        truth_particle_list.append(truth_particle)
                        #Want possible truth electrons - use vertex printing, status shoudl be=1 (stable) and hasDecayVtx=="False" and hasProdVtx=="True" and truth_el_vtx_incomingParticle=="False" and truth_el_vtx_outgoingParticle=="False" and truth_el_barcode=="200000" and trackParticle=="False" and particle_status==1 and particleId==11 -                     # if not reco_match == None:  and truth_particle.nParents()==2 and truth_particle.nChildren()==2
                        truth_particle_isTrueConvertedPhoton = ROOT.xAOD.EgammaHelpers.isTrueConvertedPhoton(truth_particle,800)
                        if abs(particleId)==11:
                            if truth_parent.nParents() and particle_status==1:
                                parent = truth_particle.parent()
                                if abs(parent.pdgId())==11 or abs(parent.pdgId())==22: 
                                    while parent.nParents()>0 and parent.barcode()<200000: 
                                        tmp = parent.parent()
                                        if abs(parent.pdgId())==11 or abs(parent.pdgId())==22:
                                            parent=tmp
                                        else:
                                            break

                                    #Obtain information about each truth electron
                                    truth_el_pt = truth_particle.pt()/GeV
                                    truth_el_eta = truth_particle.eta()
                                    truth_el_phi = truth_particle.phi()
                                    truth_el_m = truth_particle.m()/GeV
                                    total_truth_number_el.append(truth_particle)
                                    # print("truth_el_pt",truth_el_pt) 
                                    # print("truth_el_eta",truth_el_eta) 
                                    # print("truth_el_phi",truth_el_phi)  
                                    #Fill photon histograms
                                    truth_h_el_pt.Fill(truth_el_pt)
                                    truth_h_el_eta.Fill(truth_el_eta)
                                    truth_h_el_phi.Fill(truth_el_phi)   
                                    truth_el_pt_values.append(truth_el_pt)

        if(argus.datatype=="sig"):
            #Signal Sample yy->yy
            number_ph = []
            truth_particle_list = []
            # Loop over electrons
            for ph in t.Photons:
                #which truth particle matches the reco particle - can't pythonise the below
                ph_truthmatched = ROOT.xAOD.TruthHelpers.getTruthParticle(ph)
                ph_isTrueConvertedPhoton = ROOT.xAOD.EgammaHelpers.isTrueConvertedPhoton(ph,800)
                if not ph_truthmatched == None and ph_isTrueConvertedPhoton==True:
                    # print("ph_truthmatched",ph_truthmatched)
                    if abs(ph_truthmatched.pdgId())==22:
                        # print("ph_truthmatched particle", ph_truthmatched.pdgId())
                        # print("ph_truthmatched", ph_truthmatched)
                        total_number_ph.append(ph)
                        #Obtain information about each photon
                        ph_pt = ph.pt()/GeV
                        ph_eta = ph.eta()
                        ph_phi = ph.phi()
                        # print("ph_pt",ph_pt) 
                        # print("ph_eta",ph_eta) 
                        # print("ph_phi",ph_phi)    
                        ph_m = ph.m()/GeV
                        #Fill electron histograms
                        h_ph_pt.Fill(ph_pt)
                        h_ph_eta.Fill(ph_eta)
                        h_ph_phi.Fill(ph_phi)
                        ph_pt_values.append(ph_pt)
            #Truth ph

            #Once we have seen all possible vertices per event and the signal vertex we can decide if the event is one we want to store for the eff calc
            for event in range(t.TruthEvents.size()):
                TruthEvent = t.TruthEvents[event]
                trackParticle = TruthEvent.truthParticleLinks()
                # print("####################")
                # print("Will use signalProcessVertex to look most likely signal vertex....")
                TruthEvent_signalProcessVertex = TruthEvent.signalProcessVertex()
                truth_ph_vtx_In_signalProcessVertex = TruthEvent_signalProcessVertex.nIncomingParticles()
                truth_ph_vtx_Out_signalProcessVertex = TruthEvent_signalProcessVertex.nOutgoingParticles()
                # print("truth_ph_vtx_In_signalProcessVertex - nIncomingParticles for signalProcessVertex from "+str(event)+": ",truth_ph_vtx_In_signalProcessVertex)
                # print("truth_ph_vtx_Out_signalProcessVertex - nOutgoingParticles for signalProcessVertex from "+str(event)+": ",truth_ph_vtx_Out_signalProcessVertex)
                # print("Loop over signalProcessVertex incomingParticle....")
                for i in range(truth_ph_vtx_In_signalProcessVertex):
                    truth_ph_vtx_incomingParticle = TruthEvent_signalProcessVertex.incomingParticle(i)
                #     print("Particle "+str(i+1)+" - signalProcessVertex_incomingParticle type: ",truth_ph_vtx_incomingParticle.pdgId())
                #     print("Particle "+str(i+1)+" - signalProcessVertex_incomingParticle_Barcode: ",truth_ph_vtx_incomingParticle.barcode())
                #     print("Particle "+str(i+1)+" - signalProcessVertex_incomingParticle_Index: ",truth_ph_vtx_incomingParticle.index())
                #     print("Particle "+str(i+1)+" - signalProcessVertex_incomingParticle_Status",truth_ph_vtx_incomingParticle.status())
                # print("Loop over signalProcessVertex outgoingParticle")
                for i in range(truth_ph_vtx_Out_signalProcessVertex):
                    truth_ph_vtx_outgoingParticle = TruthEvent_signalProcessVertex.outgoingParticle(i)
                    # try:
                    #     print("Particle "+str(i+1)+" - signalProcessVertex_outgoingParticle:",truth_ph_vtx_outgoingParticle.pdgId())
                    #     print("Particle "+str(i+1)+" - signalProcessVertex_outingParticle_Barcode:",truth_ph_vtx_outgoingParticle.barcode())
                    #     print("Particle "+str(i+1)+" - signalProcessVertex_outingParticle_Index:",truth_ph_vtx_outgoingParticle.index())
                    #     print("Particle "+str(i+1)+" - signalProcessVertex_outingParticle_Status:",truth_ph_vtx_outgoingParticle.status())
                    # except:
                    #     print("Particle "+str(i+1)+" - attempted to access a null-particle")
                #Grab TruthVertices objects that are useful    
                for v in range(t.TruthVertices.size()):
                    #Want single vertex
                    # print("####################")
                    # print("####################")
                    # print("####################")
                    # print("vertex_number: ",v+1)
                    truth_ph_vtx = t.TruthVertices[v]
                    truth_ph_vtx_In = truth_ph_vtx.nIncomingParticles()
                    truth_ph_vtx_Out = truth_ph_vtx.nOutgoingParticles()
                    # print("truth_ph_vtx_In - nIncomingParticles for vertex "+str(v+1)+": ",truth_ph_vtx_In)
                    # print("truth_ph_vtx_Out - nOutgoingParticles for vertex "+str(v+1)+": ",truth_ph_vtx_Out)
                    # print("Loop over incomingParticle....")
                    for i in range(truth_ph_vtx_In):
                        truth_ph_vtx_incomingParticle = truth_ph_vtx.incomingParticle(i)
                    #     print("Particle "+str(i+1)+" - truth_ph_vtx_incomingParticle type: ",truth_ph_vtx_incomingParticle.pdgId())
                    #     print("Particle "+str(i+1)+" - truth_ph_vtx_incomingParticle_Barcode: ",truth_ph_vtx_incomingParticle.barcode())
                    #     print("Particle "+str(i+1)+" - truth_ph_vtx_incomingParticle_Index: ",truth_ph_vtx_incomingParticle.index())
                    #     print("Particle "+str(i+1)+" - truth_ph_vtx_incomingParticle_status",truth_ph_vtx_incomingParticle.status())

                    #     truth_ph_vtx_outgoingParticle = truth_ph_vtx.outgoingParticle(2)
                    # print("Loop over outgoingParticle")
                    for i in range(truth_ph_vtx_Out):
                        truth_ph_vtx_outgoingParticle = truth_ph_vtx.outgoingParticle(i)
                    #     try:
                    #         print("Particle "+str(i+1)+" - truth_ph_vtx_outgoingParticle",truth_ph_vtx_outgoingParticle.pdgId())
                    #         print("Particle "+str(i+1)+" - truth_ph_vtx_outingParticle_Barcode",truth_ph_vtx_outgoingParticle.barcode())
                    #         print("Particle "+str(i+1)+" - truth_ph_vtx_outingParticle_Index",truth_ph_vtx_outgoingParticle.index())
                    #         print("Particle "+str(i+1)+" - truth_ph_vtx_outingParticle_status",truth_ph_vtx_outgoingParticle.status())

                    #     except:
                    #         print("Particle "+str(i+1)+" - attempted to access a null-particle")
                    # print("####################")
                    # print("####################")

                truth_particle_number = []
                # for truth_particle in t.TruthParticles:
                for truth_particle in t.egammaTruthParticles:

                    #Grab TruthParticles objects that are useful
                    particleId = truth_particle.pdgId()
                    particle_status = truth_particle.status()
                    hasDecayVtx = truth_particle.hasDecayVtx()
                    hasProdVtx = truth_particle.hasProdVtx()
                    truth_ph_barcode = truth_particle.barcode()
                    truth_child = truth_particle.child()

                    #Create list of truth particles
                    truth_particle_list.append(truth_particle)
                    #Want possible truth photons - use vertex printing, status shoudl be=1 (stable) and hasDecayVtx=="False" and hasProdVtx=="True" and truth_ph_vtx_incomingParticle=="False" and truth_ph_vtx_outgoingParticle=="False" and truth_ph_barcode=="200000" and trackParticle=="False" and particle_status==1 and particleId==11
                    #Let's use the avaiable variables to find the truth partiucles we want particleId == 22 and truth_ph_barcode=="200000" and 
                    # print("#########")
                    # print("particleId",particleId)
                    # print("#########")  
                    truth_particle_isTrueConvertedPhoton = ROOT.xAOD.EgammaHelpers.isTrueConvertedPhoton(truth_particle,800)
                    reco_match = ROOT.xAOD.EgammaHelpers.getRecoPhoton(truth_particle) 
                    if abs(particleId)==22 and particle_status==1 and truth_ph_barcode<200000 and not reco_match==None and truth_particle_isTrueConvertedPhoton==True:
                        #Obtain information about each truth photon
                        truth_ph_pt = truth_particle.pt()/GeV
                        truth_ph_eta = truth_particle.eta()
                        truth_ph_phi = truth_particle.phi()
                        truth_ph_m = truth_particle.m()/GeV
                        total_truth_number_ph.append(truth_particle)
                        # print("truth_ph_pt",truth_ph_pt) 
                        # print("truth_ph_eta",truth_ph_eta) 
                        # print("truth_ph_phi",truth_ph_phi)  
                        #Fill photon histograms
                        truth_h_ph_pt.Fill(truth_ph_pt)
                        truth_h_ph_eta.Fill(truth_ph_eta)
                        truth_h_ph_phi.Fill(truth_ph_phi)   
                        truth_ph_pt_values.append(truth_ph_pt)

                    
    if(argus.datatype=="bkg"):
        c_el_pt_efficiency = ROOT.TCanvas("c_el_pt_efficiency", "c_el_pt_efficiency")
        h_el_pt.GetYaxis().SetTitle("Number of events")
        h_el_pt.GetXaxis().SetTitle("pT [GeV]")
        h_el_pt.SetTitle("")
        h_el_pt.SetLineColor(kBlack)
        h_el_pt.SetLineColor(kRed)
        # h_el_pt.SetMarkerStyle(kFullCircle)
        # truth_h_el_pt.SetMarkerStyle(kFullCircle)
        rp_el_pt_efficiency = ROOT.TRatioPlot(h_el_pt,truth_h_el_pt)
        rp_el_pt_efficiency.SetH1DrawOpt("kFullCircle")
        rp_el_pt_efficiency.SetH2DrawOpt("kFullCircle")
        rp_el_pt_efficiency.Draw()
        p = rp_el_pt_efficiency.GetUpperPad()
        p.BuildLegend()
        legend = ROOT.TLegend(0.55,0.65,0.65,0.80)
        legend.AddEntry(h_el_pt,"el pT","l")
        legend.AddEntry(truth_h_el_pt," truth el pT","l")
        p.Modify()
        p.Update()
        rp_el_pt_efficiency.GetLowerRefGraph().SetMinimum(0)
        rp_el_pt_efficiency.GetLowerRefGraph().SetMaximum(2)
        # truth_h_el_pt.SetOption("P")
        # c_el_pt_efficiency.SetTicks(0,1)
        c_el_pt_efficiency.Update()
        c_el_pt_efficiency.Write("c_el_pt_efficiency")

        c_el_pt_efficiency_effonly = ROOT.TCanvas("c_el_pt_efficiency_effonly", "c_el_pt_efficiency_effonly")
        rp_el_pt_efficiency.SetSplitFraction(1)
        rp_el_pt_efficiency.Draw("P")
        rp_el_pt_efficiency.GetLowerRefGraph().GetYaxis().SetTitle("Efficiency")
        # rp_el_pt_efficiency.GetLowerRefGraph().SetMinimum(0)
        # rp_el_pt_efficiency.GetLowerRefGraph().SetMaximum(1.05)
        c_el_pt_efficiency_effonly.Update()
        c_el_pt_efficiency_effonly.Write("c_el_pt_efficiency_effonly")


    if(argus.datatype=="sig"):
        c_ph_pt_efficiency = ROOT.TCanvas("c_ph_pt_efficiency", "c_ph_pt_efficiency")
        h_ph_pt.GetYaxis().SetTitle("Number of events")
        h_ph_pt.GetXaxis().SetTitle("pT [GeV]")
        h_ph_pt.SetTitle("")
        h_ph_pt.SetLineColor(kBlack)
        h_ph_pt.SetLineColor(kRed)
        # h_ph_pt.SetMarkerStyle(kFullCircle)
        # truth_h_ph_pt.SetMarkerStyle(kFullCircle)
        rp_ph_pt_efficiency = ROOT.TRatioPlot(h_ph_pt,truth_h_ph_pt)
        rp_ph_pt_efficiency.SetH1DrawOpt("kFullCircle")
        rp_ph_pt_efficiency.SetH2DrawOpt("kFullCircle")
        rp_ph_pt_efficiency.Draw()
        p = rp_ph_pt_efficiency.GetUpperPad()
        p.BuildLegend()
        legend = ROOT.TLegend(0.55,0.65,0.65,0.80)
        legend.AddEntry(h_ph_pt,"ph pT","l")
        legend.AddEntry(truth_h_ph_pt," truth ph pT","l")
        p.Modify()
        p.Update()
        rp_ph_pt_efficiency.GetLowerRefGraph().SetMinimum(0)
        rp_ph_pt_efficiency.GetLowerRefGraph().SetMaximum(2)
        # truth_h_ph_pt.SetOption("P")
        # c_ph_pt_efficiency.SetTicks(0,1)
        c_ph_pt_efficiency.Update()
        c_ph_pt_efficiency.Write("c_ph_pt_efficiency")

        c_ph_pt_efficiency_effonly = ROOT.TCanvas("c_ph_pt_efficiency_effonly", "c_ph_pt_efficiency_effonly")
        rp_ph_pt_efficiency.SetSplitFraction(1)
        rp_ph_pt_efficiency.Draw("P")
        rp_ph_pt_efficiency.GetLowerRefGraph().GetYaxis().SetTitle("Efficiency")
        # rp_ph_pt_efficiency.GetLowerRefGraph().SetMinimum(0)
        # rp_ph_pt_efficiency.GetLowerRefGraph().SetMaximum(1.05)
        c_ph_pt_efficiency_effonly.Update()
        c_ph_pt_efficiency_effonly.Write("c_ph_pt_efficiency_effonly")


    f_out.Write()
    f_out.Close()

    if(argus.datatype=="bkg"):
        total_number_el_result = len(total_number_el)
        total_truth_number_el_result = len(total_truth_number_el)
        print("total_number_el_result for "+str(argus.datatype)+"",total_number_el_result)
        print("total_truth_number_el_result for "+str(argus.datatype)+"",total_truth_number_el_result)

    if(argus.datatype=="sig"):
        total_number_ph_result = len(total_number_ph)
        total_truth_number_ph_result = len(total_truth_number_ph)
        print("total_number_ph_result for "+str(argus.datatype)+"",total_number_ph_result)
        print("total_truth_number_ph_result for "+str(argus.datatype)+"",total_truth_number_ph_result)


    if(argus.datatype=="bkg"):
        #Make a bunch of pickle dumps so we can run once and the play with plotting later if we wanted to
        with open(Directory+"Histograms/el_pt_"+str(argus.datatype)+"_events"+str(argus.numberevents)+".txt", "wb") as picklename:   #Pickling
            pickle.dump(el_pt_values,picklename)

        with open(Directory+"Histograms/truth_el_pt_"+str(argus.datatype)+"_events"+str(argus.numberevents)+".txt", "wb") as truth_picklename:   #Pickling
            pickle.dump(truth_el_pt_values,truth_picklename)
    if(argus.datatype=="sig"):
        with open(Directory+"Histograms/ph_pt_"+str(argus.datatype)+"_events"+str(argus.numberevents)+".txt", "wb") as picklename:   #Pickling
            pickle.dump(ph_pt_values,picklename)

        with open(Directory+"Histograms/truth_ph_pt_"+str(argus.datatype)+"_events"+str(argus.numberevents)+".txt", "wb") as truth_picklename:   #Pickling
            pickle.dump(truth_ph_pt_values,truth_picklename)


if(argus.pythonplot=="plot"):

    def RatioCalc(truthhist, hist, binning):
        #Create List to help calculate
        Ratio = []
        for i in range(len(binning)):
            truthhistvalue = truthhist[i]
            histvalue = hist[i]
            print(truthhistvalue) 
            print(histvalue)
            if histvalue or truthhistvalue == 0:
                Ratio.append(1)
            else:
                Ratio.append(histvalue/truthhistvalue)
        return Ratio


    if(argus.datatype=="bkg"):
        with open(Directory+"Histograms/el_pt_"+str(argus.datatype)+"_events"+str(argus.numberevents)+".txt", "rb") as picklename:   #Pickling
            el_pt = pickle.load(picklename)

        with open(Directory+"Histograms/truth_el_pt_"+str(argus.datatype)+"_events"+str(argus.numberevents)+".txt", "rb") as truth_picklename:   #Pickling
            truth_el_pt = pickle.load(truth_picklename)
    
        #Make Ratio Plots
        h_el_pt_efficiency = plt.figure()
        # set height ratios for subplots
        gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1]) 
        # the first subplot
        ax0 = plt.subplot(gs[0])
        # log scale for axis Y of the first subplot
        ax0.set_ylabel('Events', ha='right', y=1.0)
        #Histogram Binning
        el_pt_binning = numpy.linspace(0, 20, 100)
        truth_el_pt_val_bins, truth_el_pt_edges_of_bins, truth_el_pt_patches = plt.hist(truth_el_pt, bins=el_pt_binning,color = 'blue',alpha=0.5)
        el_pt_val_bins, el_pt_edges_of_bins, el_pt_patches = plt.hist(el_pt, bins=el_pt_binning,color = 'red',alpha=0.5)
        ax0.set_yscale('log')
        line1, = plt.plot(0, 0, 'blue',linewidth=2, label=r"el",alpha=0.5,zorder=0)
        line3, = plt.plot(0, 0, 'red',linewidth=2, label=r"el_truth",alpha=0.5,zorder=0)
        label = plt.plot(0, 0, ' ') 
        # ax0.set_yscale('log')
        # ax0.set_xlim(right=15)
        # ax0.set_xlim(left=0)
        # ax0.axes.xaxis.set_ticklabels([])
        # the second subplot
        # shared axis X
        ax1 = plt.subplot(gs[1], sharex = ax0)
        #Calcualte ratio to plot
        ratio_el_pt = numpy.divide(el_pt_val_bins, truth_el_pt_val_bins, where=(truth_el_pt_val_bins != 0))
        ratio_el_pt_bincenter = 0.5 * (truth_el_pt_edges_of_bins[1:] + truth_el_pt_edges_of_bins[:-1])
        ratio_el_pt_bincenter_masked = numpy.ma.masked_where(ratio_el_pt_bincenter<1,ratio_el_pt_bincenter) 
        plt.plot(ratio_el_pt_bincenter_masked,ratio_el_pt, marker='.', color="black", alpha=0.2, zorder=0)
        ax1.set_ylim(top=2)
        ax1.set_ylim(bottom=0)
        ax1.set_ylabel('Efficiency', loc='center', fontsize=18)
        ax1.set_xlabel(r'el_pt', ha='right', x=1.0)
        # remove last tick label for the second subplot
        yticks = ax1.yaxis.get_major_ticks()
        yticks[-1].label1.set_visible(False)
        # put legend on first subplot
        ax0.legend((line1, line3), ('truth el','el'))
        plt.setp(ax0.get_xticklabels(), visible=False)
        plt.subplots_adjust(hspace=.0)
        # ax3 = plt.subplot(gs[2], sharex = ax0)
        hep.atlas.label(llabel = 'Internal', year='2015-2018', fontsize=19,ax=ax0)
        h_el_pt_efficiency.savefig("Histograms/Python_ratioplot_el_pt_efficiency.pdf")

        #Make Ratio Plots
        h_el_pt_efficiency_alone = plt.figure()
        # set height ratios for subplots
        gs = gridspec.GridSpec(1, 1) 
        # the first subplot
        ax0 = plt.subplot(gs[0])
        # log scale for axis Y of the first subplot
        ax0.set_ylabel('Efficiency', ha='right', y=1.0)
        #Histogram Binning
        el_pt_binning = numpy.linspace(0, 20, 100)
        # ax0.set_xscale('log')
        line1 = plt.plot(0, 0, 'black',linewidth=2, label=r"Efficiency reco/truth",alpha=0.5,zorder=0)
        label = plt.plot(0, 0, ' ') 
        #Calcualte ratio to plot
        ratio_el_pt = numpy.divide(el_pt_val_bins, truth_el_pt_val_bins, where=(truth_el_pt_val_bins != 0))
        ratio_el_pt_bincenter = 0.5 * (truth_el_pt_edges_of_bins[1:] + truth_el_pt_edges_of_bins[:-1])
        ratio_el_pt_bincenter_masked = numpy.ma.masked_where(ratio_el_pt_bincenter<1,ratio_el_pt_bincenter) 
        plt.plot(ratio_el_pt_bincenter_masked,ratio_el_pt, marker='.', color="black", alpha=0.2, zorder=0)
        ax0.set_ylim(top=2.5)
        ax0.set_ylim(bottom=0)
        # put legend on first subplot
        ax0.legend(line1, "Efficiency reco/truth")
        hep.atlas.label(llabel = 'Internal', year='2015-2018', fontsize=19,ax=ax0)
        h_el_pt_efficiency_alone.savefig("Histograms/Python_ratioplot_el_pt_efficiency_alone.pdf")

    if(argus.datatype=="sig"):
        with open(Directory+"Histograms/ph_pt_"+str(argus.datatype)+"_events"+str(argus.numberevents)+".txt", "rb") as picklename:   #Pickling
            ph_pt = pickle.load(picklename)

        with open(Directory+"Histograms/truth_ph_pt_"+str(argus.datatype)+"_events"+str(argus.numberevents)+".txt", "rb") as truth_picklename:   #Pickling
            truth_ph_pt = pickle.load(truth_picklename)

        
        #Make Ratio Plots
        h_ph_pt_efficiency = plt.figure()
        # set height ratios for subplots
        gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1]) 
        # the first subplot
        ax0 = plt.subplot(gs[0])
        # log scale for axis Y of the first subplot
        ax0.set_ylabel('Events', ha='right', y=1.0)
        #Histogram Binning
        ph_pt_binning = numpy.linspace(0, 20, 100)
        truth_ph_pt_val_bins, truth_ph_pt_edges_of_bins, truth_ph_pt_patches = plt.hist(truth_ph_pt, bins=ph_pt_binning,color = 'blue',alpha=0.5)
        ph_pt_val_bins, ph_pt_edges_of_bins, ph_pt_patches = plt.hist(ph_pt, bins=ph_pt_binning,color = 'red',alpha=0.5)
        ax0.set_yscale('log')
        line1, = plt.plot(0, 0, 'blue',linewidth=2, label=r"ph",alpha=0.5,zorder=0)
        line3, = plt.plot(0, 0, 'red',linewidth=2, label=r"ph_truth",alpha=0.5,zorder=0)
        label = plt.plot(0, 0, ' ') 
        # ax0.set_yscale('log')
        # ax0.set_xlim(right=15)
        # ax0.set_xlim(left=0)
        # ax0.axes.xaxis.set_ticklabels([])
        # the second subplot
        # shared axis X
        ax1 = plt.subplot(gs[1], sharex = ax0)
        #Calcualte ratio to plot
        ratio_ph_pt = numpy.divide(ph_pt_val_bins, truth_ph_pt_val_bins, where=(truth_ph_pt_val_bins != 0))
        ratio_ph_pt_bincenter = 0.5 * (truth_ph_pt_edges_of_bins[1:] + truth_ph_pt_edges_of_bins[:-1])
        # plt.scatter(ph_pt_binning, ratio_ph_pt, color="black", alpha=0.2, zorder=0)
        # Mask part of array
        ratio_ph_pt_bincenter_masked = numpy.ma.masked_where(ratio_ph_pt_bincenter<1.0,ratio_ph_pt_bincenter) 
        plt.plot(ratio_ph_pt_bincenter_masked,ratio_ph_pt, marker='.', color="black", alpha=0.2, zorder=0)
        ax1.set_ylim(top=2)
        ax1.set_ylim(bottom=0)
        ax1.set_ylabel('Efficiency', loc='center', fontsize=18)
        ax1.set_xlabel(r'ph_pt', ha='right', x=1.0)
        # remove last tick label for the second subplot
        yticks = ax1.yaxis.get_major_ticks()
        yticks[-1].label1.set_visible(False)
        # put legend on first subplot
        ax0.legend((line1, line3), ('truth ph','ph'))
        plt.setp(ax0.get_xticklabels(), visible=False)
        plt.subplots_adjust(hspace=.0)
        # ax3 = plt.subplot(gs[2], sharex = ax0)
        hep.atlas.label(llabel = 'Internal', year='2015-2018', fontsize=19,ax=ax0)
        h_ph_pt_efficiency.savefig("Histograms/Python_ratioplot_ph_pt_efficiency.pdf")


        #Make Ratio Plots
        h_ph_pt_efficiency_alone = plt.figure()
        # set height ratios for subplots
        gs = gridspec.GridSpec(1, 1) 
        # the first subplot
        ax0 = plt.subplot(gs[0])
        # log scale for axis Y of the first subplot
        ax0.set_ylabel('Efficiency', ha='right', y=1.0)
        #Histogram Binning
        ph_pt_binning = numpy.linspace(0, 20, 100)
        # ax0.set_xscale('log')
        line1 = plt.plot(0, 0, 'black',linewidth=2, label=r"Efficiency reco/truth",alpha=0.5,zorder=0)
        label = plt.plot(0, 0, ' ') 
        #Calcualte ratio to plot
        ratio_ph_pt = numpy.divide(ph_pt_val_bins, truth_ph_pt_val_bins, where=(truth_ph_pt_val_bins != 0))
        ratio_ph_pt_bincenter = 0.5 * (truth_ph_pt_edges_of_bins[1:] + truth_ph_pt_edges_of_bins[:-1])
        ratio_ph_pt_bincenter_masked = numpy.ma.masked_where(ratio_ph_pt_bincenter<1.0,ratio_ph_pt_bincenter) 
        plt.plot(ratio_ph_pt_bincenter_masked,ratio_ph_pt, marker='.', color="black", alpha=0.2, zorder=0)
        ax0.set_ylim(top=2.5)
        ax0.set_ylim(bottom=0)
        # put legend on first subplot
        ax0.legend()
        hep.atlas.label(llabel = 'Internal', year='2015-2018', fontsize=19,ax=ax0)
        h_ph_pt_efficiency_alone.savefig("Histograms/Python_ratioplot_ph_pt_efficiency_alone.pdf")

